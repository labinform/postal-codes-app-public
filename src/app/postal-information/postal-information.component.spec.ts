import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostalInformationComponent } from './postal-information.component';

describe('PostalInformationComponent', () => {
  let component: PostalInformationComponent;
  let fixture: ComponentFixture<PostalInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostalInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostalInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
