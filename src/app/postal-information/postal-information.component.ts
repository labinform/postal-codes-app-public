import { Component, Input, OnInit } from '@angular/core';
import { Address } from '../_models/address';
import { AddressService } from '../_services/address.service';

@Component({
  selector: 'app-postal-information',
  templateUrl: './postal-information.component.html',
  styleUrls: ['./postal-information.component.scss']
})
export class PostalInformationComponent implements OnInit {
  @Input() address: Address;

  
  public addresses: Address[];

  ngOnInit(): void {
    console.log(this.address)
  }

  

}
