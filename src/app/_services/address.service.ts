import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Address } from '../_models/address';

@Injectable({
  providedIn: 'root'
})
export class AddressService {
  
  configUrl = 'assets/addresses.json';

  constructor(
    private http: HttpClient
  ) { }

  getAddresses(): Observable<any>{
     return this.http.get(this.configUrl);
  }

  getAddress(code): Observable<any>{
    return this.http.get(this.configUrl);
 }
}
