import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Address } from '../_models/address';

import { AddressService } from '../_services/address.service';


@Injectable()
export class AddressResolver implements Resolve<any> {

  constructor(private addressService: AddressService) {}

  resolve() {
    return new Promise((resolve, reject) => {
      this.addressService.getAddresses()
      .subscribe((addresses: Address[]) => {
        return resolve(addresses);
      });
    });
  }
}
