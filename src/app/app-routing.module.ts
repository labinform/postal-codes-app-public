import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddressResolver } from './_resolvers/address.resolver';
import { AddressService } from './_services/address.service';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    resolve: {
      addresses: AddressResolver,
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AddressResolver]
})
export class AppRoutingModule { }
