import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Address } from '../_models/address';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  title = 'Postal Code Application';
  public addresses: Address[];
  public resultFilter: Address[];
  public selectedAddress: Address;
  ngOnInit(): void {
    
  }

  constructor(
    private route: ActivatedRoute,
  ){
    this.addresses = route.snapshot.data['addresses']
  }

  filterResult(code: number){
     this.resultFilter =  this.addresses.filter(r => { return r.postalCode ===  Number(code) });
     this.selectedAddress = null;
  }
  onSelect(code: Address){
    this.selectedAddress = code;
  }

}
