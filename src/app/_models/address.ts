export class Address {
    department: string;
    municipality: string;
    postalCode: number;
    neighbourhood: string;
}